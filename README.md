# Teste project

More abount CI tests:
* GITLAB CI Quick Start: https://docs.gitlab.com/ce/ci/quick_start/
* Very nice example of CI file: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/.gitlab-ci.yml
* CI Runner project: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/tree/master 
* Others: 
 * http://docs.gitlab.com/omnibus/docker/README.html 
 * https://docs.gitlab.com/ce/ci/quick_start/
 * https://docs.gitlab.com/ce/ci/docker/using_docker_build.html
* Excelent explanation abount stages: https://about.gitlab.com/2016/07/29/the-basics-of-gitlab-ci/
